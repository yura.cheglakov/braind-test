<?php

use App\Http\Controllers\EventsController;
use App\Http\Controllers\PlacesController;
use App\Http\Controllers\ShowsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('shows')->name('shows.')->group(function() {
   Route::get('', [ShowsController::class, 'list'])->name('list');
   Route::get('/{showId}/events', [EventsController::class, 'list'])
       ->where('showId', '[0-9]+')
       ->name('events');
});

Route::prefix('events')->name('events.')->group(function() {
   Route::get('/{eventId}/places', [PlacesController::class, 'list'])
       ->where('eventId', '[0-9]+')
       ->name('places');
   Route::post('/{eventId}/reserve', [PlacesController::class, 'reserve'])
       ->where('eventId', '[0-9]+')
       ->name('place.reserve');
});