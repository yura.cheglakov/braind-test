<?php

namespace App\Contracts\TicketGateway\Responses;

interface PlaceReserveResponse extends BaseResponse
{
    public function isSuccess(): bool;
    public function getReservationId(): ?string;
}