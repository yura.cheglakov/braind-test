<?php

namespace App\Contracts\TicketGateway\Responses;

use Illuminate\Support\Collection;

interface GetPlacesResponse extends BaseResponse
{
    public function getPlaces(): Collection;
}