<?php

namespace App\Contracts\TicketGateway\Responses;

use Illuminate\Support\Collection;

interface GetEventsResponse extends BaseResponse
{
    public function getEvents(): Collection;
}