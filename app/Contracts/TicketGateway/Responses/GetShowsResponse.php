<?php

namespace App\Contracts\TicketGateway\Responses;

use Illuminate\Support\Collection;

interface GetShowsResponse extends BaseResponse
{
    public function getShows(): Collection;
}