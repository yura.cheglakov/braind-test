<?php

namespace App\Contracts\TicketGateway\Responses;

use Illuminate\Http\Client\Response;

interface BaseResponse
{
    public static function fromResponse(Response $response): static;
}