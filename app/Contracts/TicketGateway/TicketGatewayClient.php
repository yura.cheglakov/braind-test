<?php

namespace App\Contracts\TicketGateway;

use App\Contracts\TicketGateway\Responses\GetEventsResponse;
use App\Contracts\TicketGateway\Responses\GetPlacesResponse;
use App\Contracts\TicketGateway\Responses\GetShowsResponse;
use App\Contracts\TicketGateway\Responses\PlaceReserveResponse;
use App\Services\TicketGateway\Exceptions\InvalidResponse;

interface TicketGatewayClient
{
    /**
     * @return GetShowsResponse
     * @throws InvalidResponse
     */
    public function fetchShows(): GetShowsResponse;

    /**
     * @param int $showId
     * @return GetEventsResponse
     * @throws InvalidResponse
     */
    public function fetchShowEvents(int $showId): GetEventsResponse;

    /**
     * @param int $eventId
     * @return GetPlacesResponse
     * @throws InvalidResponse
     */
    public function fetchEventPlaces(int $eventId): GetPlacesResponse;

    /**
     * @param int $eventId
     * @param PlaceReserve $placeReserve
     * @return PlaceReserveResponse
     * @throws InvalidResponse
     */
    public function reservePlace(int $eventId, PlaceReserve $placeReserve): PlaceReserveResponse;
}