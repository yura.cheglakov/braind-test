<?php

namespace App\Contracts\TicketGateway;

interface PlaceReserve
{
    public function getName(): string;
    public function getPlaces(): array;
}