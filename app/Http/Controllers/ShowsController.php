<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShowResource;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class ShowsController extends TicketGatewayController
{
    /**
     * @OA\Get(
     *     path="/api/shows",
     *     summary="Получение списка мероприятий",
     *     operationId="getShows",
     *     tags={"shows"},
     *     @OA\Response(
     *         response=200,
     *         description="Успешное выполнение",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                      ref="#/components/schemas/ShowResource"
     *                 )
     *             )
     *         )
     *     )
     * )
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function list(): JsonResponse|AnonymousResourceCollection
    {
        try {
            return ShowResource::collection($this->ticketGatewayService->fetchShows()->getShows());
        } catch (InvalidResponse $e) {
            Log::error("Ошибка получения списка мероприятий: {$e->getMessage()}");
        }

        return $this->emptyJsonResponse();
    }
}