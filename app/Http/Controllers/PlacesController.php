<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlaceReserveRequest;
use App\Http\Resources\PlaceReservedResource;
use App\Http\Resources\PlaceResource;
use App\Services\TicketGateway\Classes\PlaceReserve;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class PlacesController extends TicketGatewayController
{
    /**
     * @OA\Get(
     *     path="/api/events/{eventId}/places",
     *     summary="Получение списка мест события",
     *     operationId="getEventPlaces",
     *     tags={"events"},
     *     @OA\Parameter(
     *         name="eventId",
     *         description="Идентификатор события",
     *         example=1,
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешное выполнение",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                      ref="#/components/schemas/PlaceResource"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Событие не найдено",
     *     )
     * )
     * @param int $eventId
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function list(int $eventId): JsonResponse|AnonymousResourceCollection
    {
        try {
            return PlaceResource::collection($this->ticketGatewayService->fetchEventPlaces($eventId)->getPlaces());
        } catch (InvalidResponse $e) {
            Log::error("Ошибка получения списка мест события: {$e->getMessage()}");
        }

        return $this->emptyJsonResponse();
    }

    /**
     * @OA\Post(
     *     path="/api/events/{eventId}/reserve",
     *     summary="Резервирование мест события",
     *     operationId="reserveEventPlaces",
     *     tags={"events"},
     *     @OA\Parameter(
     *         name="eventId",
     *         description="Идентификатор события",
     *         example=1,
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             ref="#/components/schemas/PlaceReserveRequest"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешное выполнение",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                      ref="#/components/schemas/PlaceReservedResource"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Событие не найдено",
     *     )
     * )
     * @param int $eventId
     * @param PlaceReserveRequest $request
     * @return PlaceReservedResource|JsonResponse
     */
    public function reserve(int $eventId, PlaceReserveRequest $request): JsonResponse|PlaceReservedResource
    {
        try {
            $response = $this->ticketGatewayService->reservePlace(
                $eventId,
                new PlaceReserve(...$request->only(['name', 'places']))
            );

            return new PlaceReservedResource($response);
        } catch (InvalidResponse $e) {
            Log::error("Ошибка резервирования мест события: {$e->getMessage()}");
        }

        return $this->emptyJsonResponse();
    }
}