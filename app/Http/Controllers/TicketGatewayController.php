<?php

namespace App\Http\Controllers;

use App\Services\TicketGateway\TicketGatewayService;
use Illuminate\Http\JsonResponse;

class TicketGatewayController extends Controller
{
    public function __construct(
        protected TicketGatewayService $ticketGatewayService
    ) {
    }

    protected function emptyJsonResponse(): JsonResponse
    {
        return response()->json(['success' => false, 'data' => []]);
    }
}