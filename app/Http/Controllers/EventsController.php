<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class EventsController extends TicketGatewayController
{
    /**
     * @OA\Get(
     *     path="/api/shows/{showId}/events",
     *     summary="Получение списка событий мероприятия",
     *     operationId="getShowEvents",
     *     tags={"shows"},
     *     @OA\Parameter(
     *         name="showId",
     *         description="Идентификатор мероприятия",
     *         example=1,
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Успешное выполнение",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                      ref="#/components/schemas/EventResource"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Мероприятие не найдено",
     *     )
     * )
     * @param int $showId
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function list(int $showId): JsonResponse|AnonymousResourceCollection
    {
        try {
            return EventResource::collection($this->ticketGatewayService->fetchShowEvents($showId)->getEvents());
        } catch (InvalidResponse $e) {
            Log::error("Ошибка получения мероприятий: {$e->getMessage()}");
        }

        return $this->emptyJsonResponse();
    }
}