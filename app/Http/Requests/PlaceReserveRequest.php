<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     @OA\Xml(name="PlaceReserveRequest")
 * )
 */
class PlaceReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @OA\Property(property="name", type="string")
     * @OA\Property(property="places", type="array",
     *   @OA\Items(type="number", example="1")
     * )
     *
     * @return array<string, Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'places' => 'required|array',
            'places.*' => 'required|numeric'
        ];
    }
}
