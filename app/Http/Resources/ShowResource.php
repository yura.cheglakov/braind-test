<?php

namespace App\Http\Resources;

use App\Services\TicketGateway\Classes\Show;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;


/**
 * @mixin Show
 *
 * @OA\Schema(
 *     @OA\Xml(name="ShowResource")
 * )
 */
class ShowResource extends JsonResource
{
    /**
     *
     * @OA\Property(property="id", type="number")
     * @OA\Property(property="name", type="string")
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName()
        ];
    }
}
