<?php

namespace App\Http\Resources;

use App\Contracts\TicketGateway\Responses\PlaceReserveResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;


/**
 * @mixin PlaceReserveResponse
 *
 * @OA\Schema(
 *     @OA\Xml(name="PlaceReservedResource")
 * )
 */
class PlaceReservedResource extends JsonResource
{
    /**
     *
     * @OA\Property(property="success", type="bool")
     * @OA\Property(property="reservationId", type="string", nullable=true)
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'success' => $this->isSuccess(),
            'reservationId' => $this->getReservationId()
        ];
    }
}
