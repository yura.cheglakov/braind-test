<?php

namespace App\Http\Resources;

use App\Services\TicketGateway\Classes\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;


/**
 * @mixin Event
 *
 * @OA\Schema(
 *     @OA\Xml(name="EventResource")
 * )
 */
class EventResource extends JsonResource
{
    /**
     *
     * @OA\Property(property="id", type="number")
     * @OA\Property(property="showId", type="number")
     * @OA\Property(property="date", type="date")
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->getId(),
            'showId' => $this->getShowId(),
            'date' => $this->getDate()
        ];
    }
}
