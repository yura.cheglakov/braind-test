<?php

namespace App\Http\Resources;

use App\Services\TicketGateway\Classes\Place;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;


/**
 * @mixin Place
 *
 * @OA\Schema(
 *     @OA\Xml(name="PlaceResource")
 * )
 */
class PlaceResource extends JsonResource
{
    /**
     *
     * @OA\Property(property="id", type="number")
     * @OA\Property(property="x", type="number")
     * @OA\Property(property="y", type="number")
     * @OA\Property(property="width", type="number")
     * @OA\Property(property="height", type="number")
     * @OA\Property(property="isAvailable", type="bool")
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->getId(),
            'x' => $this->getX(),
            'y' => $this->getY(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'isAvailable' => $this->isAvailable()
        ];
    }
}
