<?php

namespace App\Providers;

use App\Services\TicketGateway\Client\V1\TicketGatewayClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(\App\Contracts\TicketGateway\TicketGatewayClient::class, TicketGatewayClient::class);

        $this->app->when(TicketGatewayClient::class)
            ->needs('$token')
            ->giveConfig('services.ticket_gateway.token');
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }
}
