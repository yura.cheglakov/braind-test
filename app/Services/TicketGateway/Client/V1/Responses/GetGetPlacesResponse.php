<?php

namespace App\Services\TicketGateway\Client\V1\Responses;

use App\Services\TicketGateway\Classes\Place;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use App\Services\TicketGateway\Traits\CheckResponse;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class GetGetPlacesResponse implements \App\Contracts\TicketGateway\Responses\GetPlacesResponse
{
    use CheckResponse;

    protected function __construct(
        protected readonly Collection $places
    ) {
    }

    /**
     * @throws InvalidResponse
     */
    public static function fromResponse(Response $response): static
    {
        $data = self::checkResponse($response);

        return new self(collect(Arr::get($data, 'response'))->map(fn($item) => new Place(
            Arr::get($item, 'id'),
            Arr::get($item, 'x'),
            Arr::get($item, 'y'),
            Arr::get($item, 'width'),
            Arr::get($item, 'height'),
            Arr::get($item, 'is_available', false)
        )));
    }

    public function getPlaces(): Collection
    {
        return $this->places;
    }
}