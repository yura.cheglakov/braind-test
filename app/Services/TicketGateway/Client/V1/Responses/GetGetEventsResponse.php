<?php

namespace App\Services\TicketGateway\Client\V1\Responses;

use App\Services\TicketGateway\Classes\Event;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use App\Services\TicketGateway\Traits\CheckResponse;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class GetGetEventsResponse implements \App\Contracts\TicketGateway\Responses\GetEventsResponse
{
    use CheckResponse;

    protected function __construct(
        protected readonly Collection $events
    ) {
    }

    /**
     * @throws InvalidResponse
     */
    public static function fromResponse(Response $response): static
    {
        $data = self::checkResponse($response);

        return new self(collect(Arr::get($data, 'response'))->map(fn($item) => new Event(
            Arr::get($item, 'id'),
            Arr::get($item, 'showId'),
            Carbon::parse(Arr::get($item, 'date'))
        )));
    }

    /**
     * @return Collection<Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }
}