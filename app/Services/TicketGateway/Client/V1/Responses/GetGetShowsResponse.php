<?php

namespace App\Services\TicketGateway\Client\V1\Responses;

use App\Services\TicketGateway\Classes\Show;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use App\Services\TicketGateway\Traits\CheckResponse;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class GetGetShowsResponse implements \App\Contracts\TicketGateway\Responses\GetShowsResponse
{
    use CheckResponse;

    protected function __construct(
        protected readonly Collection $shows
    ) {
    }

    /**
     * @param Response $response
     * @return GetGetShowsResponse
     * @throws InvalidResponse
     */
    public static function fromResponse(Response $response): static
    {
        $data = self::checkResponse($response);

        return new self(collect(Arr::get($data, 'response'))->map(fn($item) => new Show(...$item)));
    }

    /**
     * @return Collection<Show>
     */
    public function getShows(): Collection
    {
        return $this->shows;
    }
}