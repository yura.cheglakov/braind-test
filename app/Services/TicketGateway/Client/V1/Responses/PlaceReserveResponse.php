<?php

namespace App\Services\TicketGateway\Client\V1\Responses;

use App\Services\TicketGateway\Exceptions\InvalidResponse;
use App\Services\TicketGateway\Traits\CheckResponse;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;

class PlaceReserveResponse implements \App\Contracts\TicketGateway\Responses\PlaceReserveResponse
{
    use CheckResponse;

    protected function __construct(
        protected bool $success,
        protected ?string $reservationId = null
    ) {
    }

    /**
     * @throws InvalidResponse
     */
    public static function fromResponse(Response $response): static
    {
        $data = self::checkResponse($response);

        return new self(
            Arr::get($data, 'response.success', false),
            Arr::get($data, 'response.reservation_id')
        );
    }

    /**
     * @return string|null
     */
    public function getReservationId(): ?string
    {
        return $this->reservationId;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }
}