<?php

namespace App\Services\TicketGateway\Client\V1;

use App\Contracts\TicketGateway\PlaceReserve;
use App\Contracts\TicketGateway\Responses\PlaceReserveResponse;
use App\Services\TicketGateway\Client\V1\Responses\GetGetEventsResponse;
use App\Services\TicketGateway\Client\V1\Responses\GetGetPlacesResponse;
use App\Services\TicketGateway\Client\V1\Responses\GetGetShowsResponse;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class TicketGatewayClient implements \App\Contracts\TicketGateway\TicketGatewayClient
{
    protected PendingRequest $request;

    public function __construct(string $token)
    {
        $this->request = Http::baseUrl("https://leadbook.ru/test-task-api")->withHeaders([
            'Authorization' => "Bearer $token"
        ]);
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchShows(): GetGetShowsResponse
    {
        return GetGetShowsResponse::fromResponse($this->request->get('/shows'));
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchShowEvents(int $showId): GetGetEventsResponse
    {
        return GetGetEventsResponse::fromResponse($this->request->get("/shows/$showId/events"));
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchEventPlaces(int $eventId): GetGetPlacesResponse
    {
        return GetGetPlacesResponse::fromResponse($this->request->get("/events/$eventId/places"));
    }

    /**
     * @throws InvalidResponse
     */
    public function reservePlace(int $eventId, PlaceReserve $placeReserve): PlaceReserveResponse
    {
        return Responses\PlaceReserveResponse::fromResponse(
            $this->request->asForm()->post("/events/$eventId/reserve", [
                'name' => $placeReserve->getName(),
                'places' => $placeReserve->getPlaces()
            ])
        );
    }
}