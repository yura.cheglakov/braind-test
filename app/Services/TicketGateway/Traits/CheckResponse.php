<?php

namespace App\Services\TicketGateway\Traits;

use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\Client\Response;

trait CheckResponse
{
    /**
     * @throws InvalidResponse
     */
    protected static function checkResponse(Response $response): array
    {
        if (!$response->successful()) {
            throw new InvalidResponse($response->body(), $response->status());
        }

        $data = $response->json();

        if (!is_array($data)) {
            throw new InvalidResponse($response->body(), $response->status());
        }

        return $data;
    }
}