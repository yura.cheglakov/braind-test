<?php

namespace App\Services\TicketGateway;

use App\Contracts\TicketGateway\PlaceReserve;
use App\Contracts\TicketGateway\Responses\GetEventsResponse;
use App\Contracts\TicketGateway\Responses\GetPlacesResponse;
use App\Contracts\TicketGateway\Responses\GetShowsResponse;
use App\Contracts\TicketGateway\Responses\PlaceReserveResponse;
use App\Contracts\TicketGateway\TicketGatewayClient;
use App\Services\TicketGateway\Exceptions\InvalidResponse;

class TicketGatewayService
{
    public function __construct(
        protected TicketGatewayClient $ticketGatewayClient
    ) {
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchShows(): GetShowsResponse
    {
        return $this->ticketGatewayClient->fetchShows();
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchShowEvents(int $showId): GetEventsResponse
    {
        return $this->ticketGatewayClient->fetchShowEvents($showId);
    }

    /**
     * @throws InvalidResponse
     */
    public function fetchEventPlaces(int $eventId): GetPlacesResponse
    {
        return $this->ticketGatewayClient->fetchEventPlaces($eventId);
    }

    /**
     * @throws InvalidResponse
     */
    public function reservePlace(int $eventId, PlaceReserve $placeReserve): PlaceReserveResponse
    {
        return $this->ticketGatewayClient->reservePlace($eventId, $placeReserve);
    }
}