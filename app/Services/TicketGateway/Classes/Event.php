<?php

namespace App\Services\TicketGateway\Classes;

use Illuminate\Support\Carbon;

class Event
{
    /**
     * @param int $id
     * @param int $showId
     * @param Carbon $date Дата события
     */
    public function __construct(
        protected readonly int $id,
        protected readonly int $showId,
        protected readonly Carbon $date
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getShowId(): int
    {
        return $this->showId;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }
}