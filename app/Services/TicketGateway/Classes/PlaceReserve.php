<?php

namespace App\Services\TicketGateway\Classes;

class PlaceReserve implements \App\Contracts\TicketGateway\PlaceReserve
{
    public function __construct(
        protected string $name,
        protected array $places
    ) {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getPlaces(): array
    {
        return $this->places;
    }
}