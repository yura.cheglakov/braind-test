<?php

namespace App\Services\TicketGateway\Classes;

class Place
{
    public function __construct(
        protected readonly int $id,
        protected readonly float $x,
        protected readonly float $y,
        protected readonly float $width,
        protected readonly float $height,
        protected readonly bool $isAvailable
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->isAvailable;
    }
}