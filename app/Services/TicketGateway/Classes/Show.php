<?php

namespace App\Services\TicketGateway\Classes;

class Show
{
    public function __construct(
        protected readonly int $id,
        protected readonly string $name
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}