<?php

namespace Tests\Services\TicketGateway\Client\V1;

use App\Services\TicketGateway\Classes\PlaceReserve;
use App\Services\TicketGateway\Client\V1\TicketGatewayClient;
use App\Services\TicketGateway\Exceptions\InvalidResponse;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class TicketGatewayClientTest extends TestCase
{

    /**
     * @throws InvalidResponse
     */
    public function testSuccessFetchShows()
    {
        Http::fake([
            'https://leadbook.ru/test-task-api/shows' => Http::response(
                File::get(base_path('tests/Services/TicketGateway/Client/V1/stubs/show-success.json'))
            )
        ]);

        /** @var \App\Contracts\TicketGateway\TicketGatewayClient $client */
        $client = App::make(TicketGatewayClient::class);

        $shows = $client->fetchShows()->getShows();

        $this->assertIsIterable($shows);
        $this->assertCount(15, $shows);
        $this->assertEquals(991, $shows->first()->getId());
    }

    /**
     * @throws InvalidResponse
     */
    public function testSuccessFetchEvents()
    {
        Http::fake([
            'https://leadbook.ru/test-task-api/shows/1/events' => Http::response(
                File::get(base_path('tests/Services/TicketGateway/Client/V1/stubs/events-success.json'))
            ),
            'https://leadbook.ru/test-task-api/shows/*/events' => Http::response(status: 404)
        ]);

        /** @var \App\Contracts\TicketGateway\TicketGatewayClient $client */
        $client = App::make(TicketGatewayClient::class);

        $events = $client->fetchShowEvents(1)->getEvents();

        $this->assertIsIterable($events);
        $this->assertCount(5, $events);
        $this->assertEquals("2023-03-09T17:32:15.000000Z", $events->get(3)->getDate()->toISOString());

        $this->expectException(InvalidResponse::class);
        $events = $client->fetchShowEvents(2);
    }

    /**
     * @throws InvalidResponse
     */
    public function testSuccessFetchPlaces()
    {
        Http::fake([
            'https://leadbook.ru/test-task-api/events/1/places' => Http::response(
                File::get(base_path('tests/Services/TicketGateway/Client/V1/stubs/places-success.json'))
            ),
        ]);

        /** @var \App\Contracts\TicketGateway\TicketGatewayClient $client */
        $client = App::make(TicketGatewayClient::class);

        $places = $client->fetchEventPlaces(1)->getPlaces();

        $this->assertIsIterable($places);
        $this->assertCount(24, $places);
        $this->assertEquals(360, $places->get(12)->getY());
    }

    /**
     * @throws InvalidResponse
     */
    public function testSuccessReservePlaces()
    {
        Http::fake([
            'https://leadbook.ru/test-task-api/events/1/reserve' => Http::response(
                File::get(base_path('tests/Services/TicketGateway/Client/V1/stubs/place-reserve-success.json'))
            ),
        ]);

        /** @var \App\Contracts\TicketGateway\TicketGatewayClient $client */
        $client = App::make(TicketGatewayClient::class);

        $response = $client->reservePlace(1, new PlaceReserve("ФИО", [1, 2, 3]));

        Http::assertSent(function (Request $request) {
            return is_array($request['places']) &&
                $request['places'][1] === 2 &&
                $request['name'] === "ФИО";
        });

        $this->assertEquals(true, $response->isSuccess());
        $this->assertEquals("123456789876qc", $response->getReservationId());
    }
}
