Чтобы запустить проект
1. Должна быть установлена версия PHP 8.2
2. composer install
3. cp .env.example .env
4. php artisan key:generate
5. php artisan serve